package br.ucsal.ed20202.atividade.recursividade.exercicio03;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

class ConversaoBinariaTest extends TestCase{

	@Test
	void testCalculaMDC() {
		assertEquals(10011100 , ConversaoBinaria.converterValor(156));
		assertEquals(111 , ConversaoBinaria.converterValor(7));
		assertEquals(1111 , ConversaoBinaria.converterValor(15));
	}
}
