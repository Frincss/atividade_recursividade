package br.ucsal.ed20202.atividade.recursividade.exercicio04;

public class somarPosicoes {
	static String valor;
	static int posicao = 0;

	public static int somarPosicoesAlgarismos(int x) {
		String valor2;
		
		iniciarVariaveis(x);
		posicao--;
		
		if (posicao > 0) 
			return valor.charAt(posicao) - 48 + somarPosicoesAlgarismos(x);
		else {
			valor2 = valor;
			reiniciarVariaveis();
		}
		
		return valor2.charAt(0) - 48;
	}

	private static void iniciarVariaveis(int x) {
		if (verificarVariaveis()) {
			valor = x + ""; 
			posicao = valor.length();
		}
	}
	
	private static void reiniciarVariaveis() {
		if (!verificarVariaveis()) {
			valor = null; 
			posicao = 0;
		}
	}

	private static boolean verificarVariaveis() {
		if(valor != null || posicao != 0)
			return false;
		return true;
	}

	public static void main(String[] args) {
		
		System.out.println(somarPosicoesAlgarismos(2020));
		System.out.println(somarPosicoesAlgarismos(1005));
		System.out.println(somarPosicoesAlgarismos(1932));
	}
}
