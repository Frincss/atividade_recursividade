package br.ucsal.ed20202.atividade.recursividade.exercicio04;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

class somarPosicoesTest extends TestCase{

	@Test
	void testCalculaMDC() {
		assertEquals(4 , somarPosicoes.somarPosicoesAlgarismos(2020));
		assertEquals(6 , somarPosicoes.somarPosicoesAlgarismos(1005));		
		assertEquals(19 , somarPosicoes.somarPosicoesAlgarismos(1981));
	}
}
